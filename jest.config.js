module.exports = {
  testEnvironment: "node",
  transform: { "[.]ts$": "ts-jest" },
  coverageDirectory: "coverage",
  collectCoverageFrom: ["src/**/*.ts"],
};
