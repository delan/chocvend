import { productToString } from "./product";

test("productToString returns correct values", () =>
  void expect(productToString({ name: "Caramel", cents: 250 })).toBe(
    "Caramel ($2.50)",
  ));
