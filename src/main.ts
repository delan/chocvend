import { createInterface } from "readline";

import { getChoices } from "./ui";
import { sell, centsToString } from "./money";

async function main() {
  const ui = createInterface({
    input: process.stdin,
    output: process.stdout,
  });

  let { cents, product } = await getChoices(ui);

  if (product != null) {
    try {
      cents = sell(cents, product);
    } catch (_) {
      console.log("You don’t have enough money for that!");
    }
  }

  console.log(`Returning ${centsToString(cents)} change.`);
  ui.close();
}

main();
