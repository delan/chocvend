import { Product } from "./product";

export interface Coin {
  name: string;
  cents: number;
}

/// Returns the name of the given Coin.
export function coinToString(coin: Coin): string {
  return coin.name;
}

/// Returns the given number of cents in −$1.23[4...] format.
export function centsToString(value: number): string {
  if (Number.isNaN(value) || Math.abs(value) == Infinity) {
    throw new RangeError();
  }

  const sign = value < 0 ? "−" : "";
  value = Math.abs(value);

  const dollars = Math.trunc(value / 100);
  value %= 100;

  const cents = `${Math.trunc(value)}`.padStart(2, "0");
  value %= 1;

  const fraction = `${value}`.slice(2);
  return `${sign}\$${dollars}.${cents}${fraction}`;
}

/// Returns cents of change after selling the given Product for the given
/// amount, or throws RangeError if the amount was insufficient.
export function sell(cents: number, product: Product): number {
  if (cents < product.cents) {
    throw new RangeError();
  }

  return cents - product.cents;
}
