import { centsToString, sell } from "./money";

test("centsToString returns correct values", () => {
  // catch bad sign placement or incorrect % operator usage
  expect(centsToString(-101)).toBe("−$1.01");

  // catch incorrect leading/trailing zeros on cents
  expect(centsToString(0)).toBe("$0.00");
  expect(centsToString(9)).toBe("$0.09");
  expect(centsToString(10)).toBe("$0.10");
  expect(centsToString(99)).toBe("$0.99");
  expect(centsToString(100)).toBe("$1.00");

  // catch fractional cents
  expect(centsToString(0.5)).toBe("$0.005");

  // catch unusual number values
  expect(() => centsToString(NaN)).toThrow(RangeError);
  expect(() => centsToString(Infinity)).toThrow(RangeError);
  expect(() => centsToString(-Infinity)).toThrow(RangeError);
});

test("sell returns correct change", () => {
  expect(sell(260, { name: "Caramel", cents: 250 })).toBe(10);
  expect(sell(250, { name: "Caramel", cents: 250 })).toBe(0);
});

test("sell throws RangeError when given cents are insufficient", () =>
  void expect(() => sell(249, { name: "Caramel", cents: 250 })).toThrow(
    RangeError,
  ));
