import { Coin } from "./money";
import { Product } from "./product";

export const COINS: Coin[] = [
  { name: "10¢", cents: 10 },
  { name: "20¢", cents: 20 },
  { name: "50¢", cents: 50 },
  { name: "$1", cents: 100 },
  { name: "$2", cents: 200 },
];

export const PRODUCTS: Product[] = [
  { name: "Caramel", cents: 250 },
  { name: "Hazelnut", cents: 310 },
  { name: "Organic Raw", cents: 200 },
];
