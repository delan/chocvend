import { Interface } from "readline";

import { COINS, PRODUCTS } from "./config";
import { coinToString, centsToString } from "./money";
import { Product, productToString } from "./product";

export interface Choices {
  cents: number;
  product: Product | null;
}

/// Uses the given Interface to prompt the user for some coins and
/// (optionally) the product they want to buy.
export async function getChoices(ui: Interface): Promise<Choices> {
  let cents = 0;
  let coin = null;

  printChoices(COINS.map(coinToString));
  console.log();

  while (true) {
    coin = await getChoice(
      ui,
      `Insert a coin`,
      COINS.length,
      `You have ${centsToString(cents)}.`,
    );
    console.log();

    if (coin == null) {
      break;
    }

    cents += COINS[coin].cents;
  }

  printChoices(PRODUCTS.map(productToString));
  console.log();

  const choice = await getChoice(ui, "Choose a product", PRODUCTS.length);
  const product = choice != null ? PRODUCTS[choice] : null;
  console.log();

  return { cents, product };
}

/// Prints the given choices with 1-based numbering.
function printChoices(choices: string[]) {
  console.log(choices.map((x, i) => `[${i + 1}] ${x}`).join("\n"));
}

/// Uses the given Interface to prompt the user to make a choice
/// between 1 and the given count inclusive, returning the choice
/// or null if their input was empty.
///
/// This function will retry until the user provides a valid or
/// empty input. If an introduction was given, we print it once,
/// before the prompt.
async function getChoice(
  ui: Interface,
  prompt: string,
  count: number,
  introduction?: string,
): Promise<number | null> {
  if (introduction != null) {
    console.log(introduction);
  }

  while (true) {
    try {
      return await getChoiceOnce(ui, prompt, count);
    } catch (_) {}
  }
}

/// Uses the given Interface to prompt the user to make a choice
/// between 1 and the given count inclusive, returning the choice
/// or null if their input was empty, or throws RangeError if their
/// input was neither valid nor empty.
function getChoiceOnce(
  ui: Interface,
  prompt: string,
  count: number,
): Promise<number | null> {
  return new Promise((resolve, reject) => {
    ui.question(`${prompt} (press enter for none): `, input => {
      if (input == "") {
        resolve(null);
      }

      const result = parseInt(input, 10) - 1;

      if (Number.isNaN(result) || result < 0 || result >= count) {
        reject(new RangeError());
      }

      resolve(result);
    });
  });
}
