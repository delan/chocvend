import { centsToString } from "./money";

export interface Product {
  name: string;
  cents: number;
}

/// Returns the name and price of the given Product.
export function productToString(product: Product): string {
  return `${product.name} (${centsToString(product.cents)})`;
}
