chocvend
========


This is an exercise for a chocolate bar vending machine.


To save time, it’s based on [jsynowiec/node-typescript-boilerplate],
which was written by [Jakub Synowiec] and released under the Apache
License 2.0 (see [LICENSE.boilerplate.txt]).


[jsynowiec/node-typescript-boilerplate]: https://github.com/jsynowiec/node-typescript-boilerplate
[Jakub Synowiec]: mailto:github@jakubsynowiec.info
[LICENSE.boilerplate.txt]: LICENSE.boilerplate.txt


To get started (see [package.json] for other options):


    npm install
    npm run start


[package.json]: package.json
